#!/bin/sh
# For "f_log"
source /etc/upstart/functions

f_log I dashboard "##### Starting dashboard "

# Wakeup periods in seconds
WAKEUP_PERIOD=60
RELOAD_PERIOD=1800
LAST_RELOAD_TSTAMP=0

DISPLAY_FULL_REFRESH_PERIOD=600
LAST_DISPLAY_FULL_REFRESH_TSTAMP=0

wifi_enable() {
    # Enable wireless
    f_log I dashboard "Enabling WiFi"
    lipc-set-prop com.lab126.cmd wirelessEnable 1

    PING_SUCCESSFUL=1
    RETRY_COUNT=15
    while [[ "x$PING_SUCCESSFUL" != "x0" && $RETRY_COUNT -gt 0 ]]; do
        sleep 1
        ping -w 1 -q 8.8.8.8
        PING_SUCCESSFUL=$?
        RETRY_COUNT=$(( RETRY_COUNT - 1 ))
    done

    if [[ "x$PING_SUCCESSFUL" == "x0" ]]; then
        f_log I dashboard "Successfuly connected to internet"
        echo "true"
    else
        f_log I dashboard "Failed to connect to internet"
        echo "false"
    fi
}

wifi_disable() {
    f_log I dashboard "Disabling WiFi"
    # Disable wireless
    lipc-set-prop com.lab126.cmd wirelessEnable 0
    sleep 5
}

wifi_is_dialog_shown() {
    xwininfo -tree -root | grep D_N:dialog_ID:system_PILLOW:wifi-wizard-dialog_M > /dev/null
    RESULT=$?
    if [[ "x$RESULT" == "x0" ]]; then
        echo "true"
    else
        echo "false"
    fi
}

wifi_dialog_dismiss() {
    lipc-set-prop com.lab126.pillow customDialog '{"name": "wifi_wizard_dialog", "clientParams": {"dismiss": true}}'
    sleep 2
}

browser_full_page_refresh() {
    if [[ $(wifi_enable) == "false" ]]; then
        f_log E dashboard "Failed to enable wifi for full page refresh"
        return
    fi

    f_log I dashboard "Sending refreshPage event"
    # Send refresh signal to browser
    lipc-set-prop com.PaulFreund.WebLaunch refreshPage x
    LAST_RELOAD_TSTAMP=$(date +%s)
    sleep 10

    wifi_disable
}

browser_start() {
    # Start full screen web browser
    f_log I dashboard "Starting browser"
    lipc-set-prop com.lab126.appmgrd start app://com.PaulFreund.WebLaunch
    sleep 10
}

browser_refresh_without_internet() {
    f_log I dashboard "Sending wakeupWithoutInternet event"
    KINDLE_STATUS=`kindle_status`
    # Send refresh signal to browser
    lipc-set-prop com.PaulFreund.WebLaunch wakeupWithoutInternet "$KINDLE_STATUS"
    sleep 1
}


browser_refresh_with_internet() {
    if [[ $(wifi_enable) == "false" ]]; then
        f_log E dashboard "Failed to enable wifi for page refresh with internet"
        return
    fi

    # Sync clock
    # ntpdate pool.ntp.org

    f_log I dashboard "Sending wakeupWithInternet event"
    KINDLE_STATUS=`kindle_status`
    # Send refresh signal to browser
    lipc-set-prop com.PaulFreund.WebLaunch wakeupWithInternet "$KINDLE_STATUS"
    LAST_RELOAD_TSTAMP=$(date +%s)
    sleep 15

    wifi_disable
}

browser_needs_full_refresh() {
    CURRENT_TSTAMP=$(date +%s)
    if [[ $((CURRENT_TSTAMP - LAST_RELOAD_TSTAMP)) -ge $RELOAD_PERIOD ]]; then
        echo "true"
    else
        echo "false"
    fi
}

browser_watchdog_ping() {
    lipc-set-prop com.PaulFreund.WebLaunch watchdogPing x
}

browser_is_running() {
    ACTIVE_APP=`lipc-get-prop com.lab126.appmgrd activeApp`
    if [[ $ACTIVE_APP == "com.PaulFreund.WebLaunch" ]]; then
        echo "true"
    else
        echo "false"
    fi
}

kindle_status() {
    BATTERY_CAPACITY=`power_battery_capacity`
    KINDLE_STATUS="{ \"battery\": $BATTERY_CAPACITY }"
    echo $KINDLE_STATUS
}

backlight_disable() {
    # Disable backlight
    if [[ -f /sys/class/backlight/max77696-bl/brightness ]]; then
        echo 0 > /sys/class/backlight/max77696-bl/brightness
    fi
}

backlight_enable() {
    # Disable backlight
    if [[ -f /sys/class/backlight/max77696-bl/brightness ]]; then
        echo 50 > /sys/class/backlight/max77696-bl/brightness
    fi
}

screensaver_disable() {
    # Disable screen saver
    lipc-set-prop com.lab126.powerd preventScreenSaver 1
}

splash_hide() {
    # Hide splash screen, which normally done by framework
    lipc-set-prop com.lab126.blanket unload splash
}

display_full_refresh() {
    # Refresh full display to avoid ghosting 
    eips -f ''
    LAST_DISPLAY_FULL_REFRESH_TSTAMP=$(date +%s)
    sleep 1
}

display_needs_full_refresh() {
    CURRENT_TSTAMP=$(date +%s)
    if [[ $((CURRENT_TSTAMP - LAST_DISPLAY_FULL_REFRESH_TSTAMP)) -ge $DISPLAY_FULL_REFRESH_PERIOD ]]; then
        echo "true"
    else
        echo "false"
    fi
}

power_calculate_wakeup_timestamp() {
    WAKEUP_IN_SECONDS=$1
    CURRENT_TSTAMP=$(date +%s)

    WAKEUP_TSTAMP=$((CURRENT_TSTAMP + WAKEUP_IN_SECONDS))
    # Round to nearest minute and 5 seconds, e.g. 22:44:05
    ROUNDED_WAKEUP_TSTAMP=$((CURRENT_TSTAMP + (65 - WAKEUP_TSTAMP % 60)))
    f_log I dashboard "ROUNDED_WAKE_ $ROUNDED_WAKEUP_TSTAMP"

    echo $ROUNDED_WAKEUP_TSTAMP
}

power_is_charging() {
    IS_CHARGER_PRESENT=$(/bin/cat /sys/class/power_supply/max77696-charger/present)
    if [[ "x$IS_CHARGER_PRESENT" == "x1" ]]; then
        echo "true"
    else
        echo "false"
    fi
}

power_sleep() {
    WAKEUP_TSTAMP=$1
    f_log I dashboard "WAKEUP_TSTAMP $WAKEUP_TSTAMP"

    WAKEUP_DEVICE=/dev/rtc0
    if [[ `cat /sys/class/rtc/rtc1/device/power/wakeup` == "enabled" ]]; then
        WAKEUP_DEVICE=/dev/rtc1
    fi

    CURRENT_TSTAMP=$(date +%s)
    # Workaround for rtcwake, because tstamp does not work properly
    RTCWAKE_SECS=$((WAKEUP_TSTAMP - CURRENT_TSTAMP))
    f_log I dashboard "Rtcwake in $RTCWAKE_SECS seconds"

    # Setting wakeup time takes some time
    rtcwake -s $RTCWAKE_SECS -d $WAKEUP_DEVICE
    RESULT=$?
    if [[ "x$RESULT" != "x0" ]]; then
        # Upstart will take care of restarting the service
        f_log E dashboard "Failed to put Kindle to sleep"
        exit 2
    fi
}

power_was_wakeup_manually() {
    EXPECTED_WAKEUP_TSTAMP=$1
    # Workaround for possible millisecond RTC to sys clock conversion difference
    EXPECTED_WAKEUP_TSTAMP=$((EXPECTED_WAKEUP_TSTAMP - 1))
    CURRENT_TSTAMP=$(date +%s)
    # Kindle was wakeup earlier than we set to RTC, means manual wakeup
    if [[ "$EXPECTED_WAKEUP_TSTAMP" -gt "$CURRENT_TSTAMP" ]]; then
        f_log I dashboard "Manual wakeup CURRENT_TSTAMP: $CURRENT_TSTAMP WAKEUP_TSTAMP:$EXPECTED_WAKEUP_TSTAMP"
        echo "true"
    else
        echo "false"
    fi
}

power_manual_wakeup_loop() {
    backlight_enable
    echo 0 > /tmp/touch_idle
    if [[ -c "/dev/input/event1" ]]; then
        f_log I dashboard "Starting epollidle on /dev/input/event1"
        /usr/bin/epollidle /dev/input/event1 /tmp/touch_idle &
    elif [[ -c "/dev/input/event0" ]]; then
        f_log I dashboard "Starting epollidle on /dev/input/event0"
        /usr/bin/epollidle /dev/input/event0 /tmp/touch_idle &
    else
        echo "Failed to start epollidle"
    fi

    IDLE_TIME=`cat /tmp/touch_idle`
    while [[ $IDLE_TIME -lt 60 ]]; do
        sleep 2
        IDLE_TIME=`cat /tmp/touch_idle`
    done
    f_log I dashboard "Touchscreen idle for 60 seconds, going back to sleep"

    # Kill last background task - epollidle
    kill $!
    backlight_disable
}

power_battery_capacity() {
    if [[ -f /sys/class/power_supply/max77696-battery/capacity ]]; then
        echo `cat /sys/class/power_supply/max77696-battery/capacity`
    elif [[ -f /sys/class/power_supply/bd7181x_bat/capacity ]]; then
        echo `cat /sys/class/power_supply/bd7181x_bat/capacity`
    else
        echo 0
    fi
}

init() {
    backlight_disable

    wifi_enable

    browser_start

    screensaver_disable
    splash_hide

    # Refresh browser page, just to be sure, that it is rendered correctly after screen rotation
    browser_full_page_refresh

    wifi_disable
}

main() {

    init

    while true; do

        browser_watchdog_ping
        # Watchdog will stop browser if ping is not successful,
        # so we need to start it again
        if [[ $(browser_is_running) == "false" ]]; then
            init
        fi

        # Refresh page with internet after X minutes since last refreshed
        if [[ $(browser_needs_full_refresh) == "true" ]]; then
            f_log I dashboard "Refresh with internet"
            browser_refresh_with_internet
            display_full_refresh
        else
            browser_refresh_without_internet
        fi

        # Full display refresh to avoid ghosting
        #if [[ $(display_needs_full_refresh) == "true" ]]; then
        #    display_full_refresh
        #fi

        # Dismiss wifi dialog if shown - happens occasionaly
        if [[ $(wifi_is_dialog_shown) == "true" ]]; then
            wifi_dialog_dismiss
        fi

        # Go to standby and wakeup in X seconds
        WAKEUP_TSTAMP=$(power_calculate_wakeup_timestamp $WAKEUP_PERIOD)
        power_sleep $WAKEUP_TSTAMP

        # > After wakeup, execution of script continues from here

        # If was waked manually, defer sleep
        if [[ $(power_was_wakeup_manually $WAKEUP_TSTAMP) == "true" ]]; then
            f_log I dashboard "Manual wakeup, deferring sleep"
            power_manual_wakeup_loop
        fi

    done
}

main;
