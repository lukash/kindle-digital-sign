#!/bin/sh

SCRIPT_DIR=`dirname $0`
EXTENSIONS_DIR=/mnt/us/extensions

# Remount rootfs as RW
mount -o remount,rw /

# Disable starting framework
touch /mnt/us/DONT_START_FRAMEWORK

# Copy files to proper places
cp -r $SCRIPT_DIR/WebLaunch $EXTENSIONS_DIR/
cp -r $SCRIPT_DIR/FakeHome $EXTENSIONS_DIR/
cp $SCRIPT_DIR/upstart/dashboard.conf /etc/upstart/
cp $SCRIPT_DIR/epollidle/epollidle /usr/bin/

# Execute installation of mesquite apps
sh $EXTENSIONS_DIR/WebLaunch/bin/install.sh
sh $EXTENSIONS_DIR/FakeHome/bin/install.sh
