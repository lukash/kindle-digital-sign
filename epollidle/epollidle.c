#define MAX_EVENTS 4
#define READ_SIZE 50
#include <stdio.h>     // for fprintf()
#include <unistd.h>    // for close(), read()
#include <sys/epoll.h> // for epoll_create1(), epoll_ctl(), struct epoll_event
#include <string.h>    // for strncmp
#include <fcntl.h>
#include <errno.h> // global errno, list of errors in /usr/include/asm-generic/errno-base.h
 
int main(int argc, char *argv[])
{
    if (argc != 3) {
        fprintf(stderr, "Usage:\n %s <event_file_path> <status_file>\n", argv[0]);
        return 1;
    }

    int running = 1, event_count, i;
    size_t bytes_read;
    char read_buffer[READ_SIZE + 1];
    struct epoll_event event, events[MAX_EVENTS];
    int epoll_fd = epoll_create1(0);
    int idle_period = -1;

    if (epoll_fd == -1) {
        fprintf(stderr, "Failed to create epoll file descriptor\n");
        return 1;
    }

    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "Failed to open file %d\n", errno);
        return 1;
    }

    int status_fd = open(argv[2], O_WRONLY | O_CREAT);
    if (status_fd < 0) {
        fprintf(stderr, "Failed to open status file %d\n", errno);
        return 1;
    }

    event.events = EPOLLIN;
    event.data.fd = fd;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, event.data.fd, &event)) {
        fprintf(stderr, "Failed to add file descriptor to epoll - err: %d\n", errno);
        close(epoll_fd);
        close(status_fd);
        close(fd);
        return 1;
    }

    // printf("\nStarted polling...\n");
    while (running) {
        event_count = epoll_wait(epoll_fd, events, MAX_EVENTS, 2000);
        // printf("Events come, count: %d\n", event_count);
        if (event_count > 0 && idle_period != 0) {
            for (i = 0; i < event_count; i++) {
                // just clear events
                read(events[i].data.fd, read_buffer, READ_SIZE);
            }
            idle_period = 0;
            truncate(argv[2], 0);
            write(status_fd, "0", 2);
        } else {
            idle_period += 2;
            char idle_str[10];
            sprintf(idle_str, "%d", idle_period);
            truncate(argv[2], 0);
            write(status_fd, idle_str, strlen(idle_str) + 1);
        }
    }

    close(fd);
    close(status_fd);
    if (close(epoll_fd)) {
        fprintf(stderr, "Failed to close epoll file descriptor\n");
        return 1;
    }

    return 0;
}
