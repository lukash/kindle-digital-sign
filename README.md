Kindle as a digital sign
======================
This repo contains set of startup scripts that can replace default Kindles home screen with full-screen browser. It allows to load any web page and periodically updates it. It uses power-saving features to save battery life as much as possible.

Requires jailbroken Kindle or access over serial port.

**Features:**

- Full screen browser
- Landscape mode
- No framework running
- Uses standby to save battery
- Wakes every 60 seconds (good to update clock)
- Reloads full page every 30 minutes (to update weather, ...)
- System boots directly to browser
- Sends battery status to browser

Tested on Paperwhite 2 version 5.11.1.1.

Web browser is based on following projects:
https://github.com/lucasmpr/WebLaunch/tree/master/bin
https://github.com/PaulFreund/WebLaunch/tree/master/bin

Installation
=============
1. Copy this project to Kindle over USB
2. Login to Kindle over SSH or serial port as root
3. Execute `sh /mnt/us/kindle-digital-sign/install.sh`
4. Configure web page to start in */mnt/us/extensions/WebLaunch/bin/index.html* on line with `var address = `
5. Reboot

Stopping dashboard
------------------

    stop dashboard

Debugging dashboard
-------------------
To debug script just stop service and start script manually:

    stop dashboard
    sh /mnt/us/extensions/WebLaunch/bin/dashboard-service.sh


Folders
========

epollidle
----------
Small C utility that monitors character file for changes and every 2 seconds updates idle time in defined text file. When character file is changed, it resets idle counter.
We use it to wait on Kindle touch screen input file */dev/input/event1*, to find out how long touch screen is idle. Usage:

    epollidle /dev/input/event1 /tmp/touch_idle

Directory *epollidle* contains source code and pre-compiled version for Kindle.

FakeHome
--------
App manager and Mesquite in Kindle has hardcoded home application to be "com.lab126.booklet.home". If framework is not running, home is not present in LIPC and app manager is not able to stop applications. So we create empty Mesquite application, which we will see when we stop the browser.

utils
------
Folder utils contains statically compiled `strace` command for Kindle kernel "3.0.35". Useful to reverse engineer behavior of some commands.

WebLaunch
----------
Mesquite full screen browser with few more capabilities than Kindle's built-in browser.
Start browser:

    stop framework
    lipc-set-prop com.lab126.appmgrd start app://com.PaulFreund.WebLaunch

Kill browser:

    lipc-set-prop com.PaulFreund.WebLaunch kill now

Web browser supports two special LIPC events:

    lipc-set-prop com.PaulFreund.WebLaunch refreshPage x
    lipc-set-prop com.PaulFreund.WebLaunch loadUrl "https://google.com"

Custom events:

    lipc-set-prop com.PaulFreund.WebLaunch wakeupWithoutInternet x
    lipc-set-prop com.PaulFreund.WebLaunch wakeupWithInternet x

**Logs for debugging**
Debugging problems with Mesquite applications (not starting, crashing) are in */var/log/messages*


Power source
------------
Schematic of power source with relay.


Web page requirements
======================
We want to save as much battery as we can, so Kindle is always sleeping. It wakes up every minute for a second, without internet, to refresh clock, and every 30 minutes it wakes, enables the internet and has 30 seconds to execute some data reload, before it disables the internet and goes back to sleep.

On the page loaded in browser, we can define JavaScript callbacks, that Kindle calls for updating page status.
There is only **one mandatory callback for wathdog**. It is called every minute and must return true. If it does not exists or returns false, browser is restarted. 

    function kindleWatchdogPing() {
        return true;
    }

Two other callbacks are optional:

    function kindleWakeupWithoutInternet(kindleStatus) {
        time += 1;
    }

    function kindleWakeupWithInternet() {
        location.reload();
    }

Function `kindleWakeupWithoutInternet` has optional argument `kindleStatus` which contains serialized JSON:

    {
        "battery": 87
    }


Documentation
==============

dashboard-service.sh
---------------------
Responsibilities:

- starts WebLaunch
- powers down kindle
- enables/disables wifi

Workflow
---------
![Workflow](doc/kindle-digital-sign-workflow.svg)

TODO
=====

    [ ] Wakeup on first seconds of a minute
    [ ] Show error icon when wifi not enabled
    [ ] Start WiFi on manual wakeup to enable browsing the page
